package confluence.testdata


class DataSource {
    static restrictedPageData = new PageData([
            pageTitle: "Restricted page",
            contents : "Top secret restricted content"
    ])

    static newPageData = new PageData([
            pageTitle: "Nice new page",
            contents : "not so plenty of text"
    ])

    static adminUserData = new UserData([
            name    : "Krzysztof Kępiński",
            role    : "Administrator",
            login   : 'kepinskik@gmail.com',
            password: 'SpartezExercise'
    ])
}
