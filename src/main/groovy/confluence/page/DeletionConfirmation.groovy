package confluence.page

import geb.Page


class DeletionConfirmation extends Page {

    static at = {
        waitFor {
            pageTitle.text() == "Delete Page"
        }
    }

    static content = {
        pageTitle { $("#title-text") }
        confirmButton(wait: true) { $("input#confirm") }
    }

    def confirmDeletion() {
        confirmButton.click()
    }
}
