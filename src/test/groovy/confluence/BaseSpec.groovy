package confluence

import confluence.page.DeletionConfirmation
import confluence.page.LoginPage
import confluence.page.WikiPage
import geb.spock.GebReportingSpec

class BaseSpec extends GebReportingSpec {

    def login(userData, page, String ... args) {
        via page, args
        if (browser.isAt(LoginPage)) {
            at (LoginPage).login(userData)
        }
        at page
    }

    def cleanupPage(pageData) {
        to(WikiPage, "TS", pageData.urlParam())
                .deletePage()
        at DeletionConfirmation confirmDeletion()
    }

    def cleanupPageRestrictions(pageData) {
        to(WikiPage, "TS", pageData.urlParam()).with {
            openPageRestrictions()
            restrictions.set("No restrictions")
        }
    }
}
