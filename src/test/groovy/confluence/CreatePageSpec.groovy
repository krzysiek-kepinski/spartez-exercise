package confluence

import confluence.page.AddNewPageForm
import confluence.page.Wiki
import confluence.page.WikiPage

import static confluence.testdata.DataSource.*


class CreatePageSpec extends BaseSpec {

    def setup() {
        login adminUserData, Wiki
    }

    def "create new page"() {
        given: at Wiki
        when: openNewPageForm()
        then: at AddNewPageForm
        when: publishWikiPageWith newPageData
        then: at WikiPage
        contents() == newPageData
        and:
        credits() == adminUserData.credits()
    }

    def cleanup() {
        cleanupPage newPageData
    }
}
