package confluence.testdata

import groovy.transform.EqualsAndHashCode


@EqualsAndHashCode
class UserData {
    String name
    String role
    String login
    String password

    def credits() {
        "$name [$role]"
    }
}
