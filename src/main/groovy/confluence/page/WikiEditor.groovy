package confluence.page

import geb.Page


class WikiEditor extends Page {
    static content = {
        tinymceEditor { $("body#tinymce") }
    }
}
