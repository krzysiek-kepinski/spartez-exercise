Charter: Explore Reorder Pages functionality
using incremental complexity of data
and interactions with concurrent manual page operations(delete, move)
in order to find functional defects and possible data loss related with complex data structure

1. simple reordering:
- access reorder pages when the space has no pages
- access reorder pages when the space has one page
- pull the page out of the space tree - drag it on top of the space/ under the space - small inconsistency, having a space with one page and trying to drag the page under the space does nothing
- drag the page back into the space
- drag the space onto the page - the space home is just another page!
- drag the root of the tree onto the child of that root
2. reordering with multiple pages:
- create a tree where every node has only one child(8 nodes)
- move the only leaf out of the tree
- move any other non-leaf node out of the tree
- move the leaf to become a direct child of the root - there is a children sorting feature!
- move any other non-leaf to become a direct child of the root
- sort children of a node when both of them are leaves, undo
- sort children of a node when they are already sorted, undo
- sort children of a node when one of them is not a leaf
- strange thing happened: i dragged one of the sorted leaves away and picture1 happened - notice the sorting button next to test 6 and also note that test 3 and test 7 have children arrows, while they are leaves
- sort node with 3 children that were sorted, drag one of them to a different parent outside this subtree
- inconsistency: when dragging two children onto the same node sorted, the sorting icon appears, while in the previous scenario it is gone as a result of removing one of three sorted children
3. Interactions:
- delete one of the pages in other tab and then try to drag it to a new location, try to drag other pages onto it, refresh the page.
- previous scenario caused all pages dragged to be children of the deleted page disappear both from page tree and reorder tab
- previous operation also broke node test 5 - it shows the sorting button despite having only one descendant
- move one of the parent pages with the action dropdown in other tab - it looks like wiki has some sort of inconsistency detection mechanism and notifies the user that the state if the tree has changed. This mechanism didn't work in case of delete action though.
- move one of the leaf pages with the action dropdown in other tab

Running above scenarios plus attempts to sort out possible misbehaviors = 30 minutes

Before releasing this, I'd like to dig further into:
1. the children sorting mechanism, which seems to work inconsistently in some cases, but mechanics of these inconsistencies are still unclear to me
2. interactions with parallel changes to the tree in the background - when some other user is playing with the Reorder Pages tab or is moving/deleting pages in action menu.
3. In some cases I also noticed leaves having the arrow icon next to them. The icon seems to be reserved to parents and is used to unroll the childrens list, but in this case it was stale. Further investigation is needed in order to pin down the defect.

The largest risk I notice is the potential data loss on pages that were not directly deleted, but dragged onto already deleted pages.