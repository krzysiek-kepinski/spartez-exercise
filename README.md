# README #

### Spartez excercises solutions ###

* [Ingredient #1](http://www.gebish.org/)
* [Ingredient #2](http://spockframework.org/)

### How do I get set up? ###

* Install Firefox browser
* run ```./gradlew clean test``` from the root of the repository

### Assumptions ###
* Tests require specific confluence account to be active.
* Account details can be found and changed in the confluence.testdata.DataSource class
* One of the tests requires a page titled `Restricted page` to be created on the account

### Who do I talk to? ###

kepinskik@gmail.com