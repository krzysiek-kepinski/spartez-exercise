package confluence.page

import geb.Page


class LoginPage extends Page{

    static at = {
        usernameInput.isDisplayed() &&
                passwordInput.isDisplayed()
    }

    static content = {
        usernameInput { $("input#username") }
        passwordInput { $("input#password") }
        loginButton { $("button#login") }
    }

    def login(userData) {
        usernameInput << userData.login
        passwordInput << userData.password
        loginButton.click()
    }

}
