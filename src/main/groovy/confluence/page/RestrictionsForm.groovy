package confluence.page

import geb.Module
import geb.module.Select


class RestrictionsForm extends Module {

    static content = {
        applyButton(to: WikiPage) { $("#page-restrictions-dialog-save-button") }
        cancelButton(to: WikiPage) { $("#page-restrictions-dialog-close-button") }
        applyButtonDisabled { applyButton.hasClass("disabled") }
        levelSelect { $("#page-restrictions-dialog-selector").module(Select) }
        selected { $("#s2id_page-restrictions-dialog-selector a span.title").text() }
    }

    def set(level) {
        levelSelect.setSelected(level)
        applyButtonDisabled? cancelButton.click(): applyButton.click()
    }

    def selectedLevel() {
        selected
    }
}
