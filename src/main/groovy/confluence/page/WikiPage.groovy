package confluence.page

import confluence.testdata.PageData
import geb.Page

class WikiPage extends Page {

    static url = "/wiki/display/"

    static at = {
        waitFor {
            pageTitleHeading.isDisplayed() &&
                    !$("div.aui-blanket").isDisplayed()
        }
    }

    static content = {
        pageTitleHeading { $("#title-heading") }
        pageTitleText { $("#title-text").text() }
        contentDiv { $("div#content") }
        pageContentText { contentDiv.$("#main-content p")*.text().join("\n") }
        authorCredits { contentDiv.$("span.author").text() }
        actionMenu(required: false) { $("#com-atlassian-confluence").module(ActionMenu) }
        restrictionsIcon { $("#content-metadata-page-restrictions") }
        restrictions(required: false) { $("section#update-page-restrictions-dialog").module(RestrictionsForm) }
    }

    def contents() {
        new PageData(
                [
                        pageTitle: pageTitleText,
                        contents: pageContentText
                ]
        )
    }

    def credits() {
        authorCredits
    }

    def deletePage() {
        actionMenu.delete()
    }

    def openPageRestrictions() {
        if( !restrictions.isDisplayed() ) restrictionsIcon.click()
        waitFor(10, 1) {
            restrictions.applyButton.isDisplayed()
        }
        restrictions
    }

    def restrictionsLevel() {
        openPageRestrictions().selectedLevel()
    }
}
