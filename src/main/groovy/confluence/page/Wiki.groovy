package confluence.page

import geb.Page

class Wiki extends Page {

    static url = "/wiki"

    static at = {
        logo.isDisplayed() &&
                sidebarDiscover.isDisplayed()
    }

    static content = {
        logo { $("h1#logo") }
        sidebarDiscover { $("#sidebar-discover") }
        quickCreateNewPageButton(to: AddNewPageForm) { $("#quick-create-page-button") }
    }

    def openNewPageForm() {
        quickCreateNewPageButton.click()
    }
}