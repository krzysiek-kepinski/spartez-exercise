package confluence

import confluence.page.WikiPage
import spock.lang.Unroll

import static confluence.testdata.DataSource.*


class SetPageRestrictionsSpec extends BaseSpec {

    def setup() {
        login adminUserData, WikiPage, "TS", restrictedPageData.urlParam()
    }

    @Unroll
    "set page restrictions to \'#level\'"() {
        given: at WikiPage
        when: openPageRestrictions()
        then: restrictions.selected == "No restrictions"
        when: restrictions.set(level)
        then: at WikiPage
        contents() == restrictedPageData
        and:
        restrictionsLevel() == level
        where:
        level                            | _
        "Viewing and editing restricted" | _
        "Editing restricted"             | _
    }

    def cleanup() {
        cleanupPageRestrictions restrictedPageData
    }

}
