package confluence.testdata

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class PageData {
    String pageTitle
    String contents

    def urlParam() {
        URLEncoder.encode(pageTitle, 'UTF-8')
    }
}
