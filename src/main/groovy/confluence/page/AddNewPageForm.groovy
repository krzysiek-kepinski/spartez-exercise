package confluence.page

import geb.Page

class AddNewPageForm extends Page {

    static at = {
        title.contains("Add Page")
    }

    static content = {
        publishButton(to: WikiPage) { $("#rte-button-publish") }
        pageEditForm(wait: true) { $("#createpageform").module(WikiEditionModule) }
        errorNotifications(required: false, wait: true) { $("#aui-flag-container") }
    }

    def publishWikiPageWith(pageContent) {
        pageEditForm.insert(pageContent)
        publishButton.click()
        assert errorNotifications.isEmpty(), "Unexpected error: ${errorNotifications.text()}"
    }
}
