package confluence.page

import geb.Module

class WikiEditionModule extends Module {

    static content = {
        title { $("input#content-title") }
        wysiwygEditor(page: WikiEditor) { $("#wysiwygTextarea_ifr") }
    }

    def insert(pageContent) {
        title.value(pageContent.pageTitle)
        withFrame(wysiwygEditor) {
            tinymceEditor << pageContent.contents
        }
    }

}

