package confluence.page

import geb.Module


class ActionMenu extends Module {

    static content = {
        displayToggle { $("#action-menu-link") }
        actionMenu { $("#action-menu") }
        deleteButton(to: DeletionConfirmation) { actionMenu.$("#action-remove-content-link") }
    }

    def toggleDisplay() {
        displayToggle.click()
    }

    def isActionable() {
        actionMenu.isDisplayed()
    }

    def delete() {
        if( !isActionable() ) toggleDisplay()
        waitFor { deleteButton.isDisplayed() }
        deleteButton.click()
    }
}
